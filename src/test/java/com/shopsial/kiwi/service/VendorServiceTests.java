package com.shopsial.kiwi.service;

import java.util.List;

import javax.annotation.Resource;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.shopsial.kiwi.domain.Vendor;
import com.shopsial.kiwi.repository.IVendorRepository;


@ContextConfiguration(locations="classpath:applicationContextTest.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class VendorServiceTests {
	
	@Autowired
	private MongoTemplate vendorService;

	
	@Test
	public void test() {
		vendorService.dropCollection(Vendor.class);
		Vendor v = new Vendor();
		v.setName("Hospital Vall d'Hebron");
		v.setDescription("Hospitals de Barcelona");
		v.setLocation(new double[]{42.0,2.0});
		vendorService.save(v);
		List<Vendor> results = vendorService.findAll(Vendor.class);
		Assert.assertEquals(1, results.size());
	}

}
