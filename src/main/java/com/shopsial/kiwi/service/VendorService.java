package com.shopsial.kiwi.service;

import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.shopsial.kiwi.domain.Vendor;
import com.shopsial.kiwi.repository.IVendorRepository;

/**
 * Service for processing {@link Vendor} objects.
 * <p>
 * Uses Spring Data''s {@Repository)  to perform CRUD operations.
 * 
 * @author Joan Casadellˆ & Xevi Gallego at (@link http://developer@shopsial.com)
 */

@Repository("vendorService")
@Transactional
public class VendorService {
	
	protected static Logger logger = Logger.getLogger("service");
	
	@Autowired
	private IVendorRepository vendorRepository;
	
	public List<Vendor> getAll() {
		logger.debug("Retrieving all vendors");
		return vendorRepository.findAll();
	}
	
	public Vendor get(String id) {
		logger.debug("Retrieving an existing vendor");
		return vendorRepository.findOne(id);
	}
	
	public boolean add(Vendor vendor) {
		logger.debug("Adding a new Vendor");
		
		try {
			// Set a new id property since it's blank
			vendor.setId(UUID.randomUUID().toString());
			// Insert into DB
			vendorRepository.save(vendor);
			return true;
			
		} catch (Exception e) {
			logger.error("An error has occurred while trying to add a new vendor", e);
			return false;
		}
	}
	
	public boolean delete(String id) {
		logger.debug("Deleting existing vendor");
		
		try {
			// Find an entry where id matches the id, then try delete it
			vendorRepository.delete(vendorRepository.findOne(id));
			return true;
			
		} catch (Exception e) {
			logger.error("An error has occurred while trying to delete a vendor", e);
			return false;
		}
	}
	
	public boolean update(Vendor vendor) {
		logger.debug("Updating existing vendor");
		
		try {
			// Find an entry where id matches the id
			Vendor existingVendor = vendorRepository.findOne(vendor.getId());
			
			// Update new values
			existingVendor.setName(vendor.getName());
			existingVendor.setDescription(vendor.getDescription());
			existingVendor.setLocation(vendor.getLocation());
			
			// Save to DB
			vendorRepository.save(existingVendor);
			
			return true;
			
		} catch (Exception e) {
			logger.error("An error has occurred while trying to update existing vendor", e);
			return false;
		}
	}
	
	public boolean deleteAll() {
		logger.debug("Deleting all vendors");
		
		try {
			vendorRepository.deleteAll();
			return true;
			
		} catch(Exception e) {
			logger.error("An error has occurred while trying to delete all vendors", e);
			return false;
		}
	}
	
}
