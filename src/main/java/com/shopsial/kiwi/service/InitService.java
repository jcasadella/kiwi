package com.shopsial.kiwi.service;

import java.util.UUID;

import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.shopsial.kiwi.domain.Vendor;

/**
 * Service for initializing MongoDB with sample data.
 * <p>
 * To recreate the collection, we must use {@link MongoTemplate}
 * 
 * @author Joan Casadellˆ & Xevi Gallego at (@link http://developer@shopsial.com)
 */

@Transactional
public class InitService {
	
	protected static Logger logger = Logger.getLogger("service");
	
	@Resource(name="mongoTemplate")
	private MongoTemplate mongoTemplate;
	
	public void init() {
		// Populate MongoDB
		logger.debug("Init MongoDB");
		
		// Drop existing collection
		mongoTemplate.dropCollection("vendor");
		
		// Create new object
		Vendor v = new Vendor();
		v.setId(UUID.randomUUID().toString());
		v.setName("Starbucks Uriquinaona");
		v.setDescription("Starbucks Coffee Company");
		v.setLocation(new double[]{41.388803,2.171924});
		
		// Insert to DB
		mongoTemplate.insert(v, "vendor");
		
		// Create new object
		v = new Vendor();
		v.setId(UUID.randomUUID().toString());
		v.setName("Starbucks Carrer Ferran");
		v.setDescription("Starbucks Coffee Company");
		v.setLocation(new double[]{41.381232,2.17525});
		
		// Insert to DB
		mongoTemplate.insert(v, "vendor");
		
		// Create new object
		v = new Vendor();
		v.setId(UUID.randomUUID().toString());
		v.setName("Starbucks Via Laietana");
		v.setDescription("Starbucks Coffee Company");
		v.setLocation(new double[]{41.385018,2.177433});
		
		// Insert to DB
		mongoTemplate.insert(v, "vendor");
	}
}
