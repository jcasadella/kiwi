package com.shopsial.kiwi.repository;

import com.shopsial.kiwi.domain.Vendor;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IVendorRepository extends MongoRepository<Vendor, String>{
	
	public Vendor findByName(String name);

}
