package com.shopsial.kiwi.controller;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.shopsial.kiwi.domain.Vendor;
import com.shopsial.kiwi.service.VendorService;

/**
 * Handles and retrieves vendor requests
 * <p>
 * Uses {@link VendorService} to access data
 * 
 * @author Joan Casadellˆ & Xevi Gallego at (@link http://developer@shopsial.com)
 */

@Controller
@RequestMapping("/")
public class VendorController {
	
	protected static Logger logger = Logger.getLogger("controller");
	
	@Resource(name="vendorService")
	private VendorService vendorService;
	
	/**
	 * Handles and retrieves all persons and show it in a JSP page
	 * 
	 * @return the name of the JSP page
	 */
	@RequestMapping(value = "/vendors", method = RequestMethod.GET)
	public String getVendors(Model model) {
		logger.debug("Received request to show all vendors");
		
		// Retrieve all vendors by delegating the call to VendorService
		List<Vendor> vendors = vendorService.getAll();
		
		// Attach vendors to the Model
		model.addAttribute("vendors", vendors);
		
		// This will resolve to /WEB-INF/jsp/vendors.jsp
		return "vendors";
	}
}
