<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Kiwi</title>
</head>
<body>
<h1>Vendors</h1>

<table style="border: 1px solid; width: 500px; text-align:center">
	<thead style="background:#ccf">
		<tr>
			<th>Name</th>
			<th>Description</th>
			<th>Location</th>
			<th colspan="3"></th>
		</tr>
	</thead>
	<tbody>
	<c:forEach items="${vendors}" var="vendor">
			<c:url var="editUrl" value="/vendors/edit?id=${vendor.id}" />
			<c:url var="deleteUrl" value="/vendors/delete?id=${vendor.id}" />
			<c:url var="addUrl" value="/vendors/add" />
		<tr>
			<td><c:out value="${vendor.name}" /></td>
			<td><c:out value="${vendor.description}" /></td>
			<td><c:out value="${vendor.location[0]},${vendor.location[1]}" /></td>
			<td><a href="${editUrl}">Edit</a></td>
			<td><a href="${deleteUrl}">Delete</a></td>
			<td><a href="${addUrl}">Add</a></td>
		</tr>
	</c:forEach>
	</tbody>
</table>

</body>
</html>